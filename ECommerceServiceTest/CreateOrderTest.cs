﻿using System;
using System.Collections.Generic;
using ECommerce.DataContracts;
using ECommerce.Service;
using NUnit.Framework;

namespace ECommerce.Test
{
    [TestFixture]
    public class CreateOrderTest
    {
        private ECommerceService _service;

       [SetUp]
        public void SetUpCustomers()
        {
            _service = new ECommerceService();
            Customer customer1 = new Customer
            {
                Name = "John",
                EmailId = "john@gmail.com",
                Orders = new List<Order>
                {
                    new Order
                    {
                         CreatedDate = DateTime.Now,
                         Items = new List<Product>()
                         {
                              new Product() { Name = "Monitor", Price = 80 },
                              new Product() { Name = "Keyboard", Price = 30 }
                         }
                    }
                }
            };
            Customer customer2 = new Customer
            {
                Name = "Mary",
                EmailId = "mary@gmail.com",
                Orders = new List<Order>
                {
                    new Order
                    {
                         CreatedDate = DateTime.Now,
                         Items = new List<Product>()
                         {
                              new Product() { Name = "Monitor", Price = 80 },
                              new Product() { Name = "Keyboard", Price = 30 }
                         }
                    },
                     new Order
                    {
                         CreatedDate = DateTime.Now,
                         Items = new List<Product>()
                         {
                              new Product() { Name = "Cable", Price = 5 }
                         }
                    }
                }
            };
            _service.CreateCustomer(customer1);
            _service.CreateCustomer(customer2);
        }

        [Test]
        public void CreateOrderWithInvalidCustomerIdAndNullOrderShouldFailTest()
        {
            var result = _service.CreateOrder(3,null);
            Assert.IsFalse(result);
        }

        [Test]
        public void CreateOrderWithValidCustomerIdAndNullOrderShouldFailTest()
        {
            var result = _service.CreateOrder(1, null);
            Assert.IsFalse(result);
        }

        [Test]
        public void CreateOrderWithValidCustomerIdAndEmptyOrderShouldFailTest()
        {
            Order order = new Order();
            var result = _service.CreateOrder(1, null);
            Assert.IsFalse(result);
        }

        [Test]
        public void CreateOrderWithValidCustomerIdAndOrderWithEmptyProductsShouldFailTest()
        {
            Order order = new Order()
            {
                CreatedDate = DateTime.Now
            };
            var result = _service.CreateOrder(1, order);
            Assert.IsFalse(result);
        }

        [Test]
        public void CreateOrderWithInValidCustomerIdAndValidOrderShouldFailTest()
        {
            Order order = new Order()
            {
                CreatedDate = DateTime.Now
            };
            var result = _service.CreateOrder(2, order);
            Assert.IsFalse(result);
        }

        [Test]
        public void CreateOrderWithValidCustomerIdAndValidOrderShouldPassTest()
        {
            Order order = new Order()
            {
                CreatedDate = DateTime.Now,
                Items = new List<Product>()
                {
                    new Product(){ Name = "Monitor", Price = 50},
                    new Product(){ Name = "Keyboard", Price = 20}
                }
            };
            var result = _service.CreateOrder(1, order);
            Assert.IsTrue(result);
        }
    }
}
