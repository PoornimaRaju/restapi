﻿using ECommerce.DataContracts;
using ECommerce.Service;
using NUnit.Framework;
using System.Collections.Generic;

namespace ECommerce.Test
{
    [TestFixture]
    public class CreateCustomerTest
    {
        private ECommerceService _service;

        [SetUp]
        public void Setup()
        {
            _service = new ECommerceService();
        }
        
        [Test]
        public void CreateCustomerWithNullValueShouldFailTest()
        {
            var result = _service.CreateCustomer(null);
            Assert.IsFalse(result);
        }

        [Test]
        public void CreateCustomerWithOrderWithValidPriceAndInvalidCreatedDateShouldFailTest()
        {
            Customer customer = new Customer
            {
                Name = "John",
                EmailId = "john@gmail.com",
                Orders = new List<Order>
                {
                    new Order
                    {
                         Id = 1,
                         Items = new List<Product>()
                         {
                              new Product() { Name = "Monitor", Price = 80 },
                              new Product() { Name = "Keyboard", Price = 30 }
                         }
                    }
                }
            };
            var result = _service.CreateCustomer(customer);
            Assert.IsFalse(result);
        }

        [Test]
        public void CreateCustomerWithOrderWithInValidPriceAndValidCreatedDateShouldFailTest()
        {
            Customer customer = new Customer
            {
                Name = "John",
                EmailId = "john@gmail.com",
                Orders = new List<Order>
                {
                    new Order
                    {
                         Id = 1,
                         CreatedDate = new System.DateTime(2018,01,12),
                         Items = new List<Product>()
                         {
                              new Product() { Name = "Monitor" },
                              new Product() { Name = "Keyboard"}
                         }
                    }
                }
            };
            var result = _service.CreateCustomer(customer);
            Assert.IsFalse(result);
        }

        [Test]
        public void CreateCustomerWithOrderWithValidPriceAndValidCreatedDateShouldSucceededTest()
        {
            Customer customer = new Customer
            {
                Name = "John",
                EmailId = "john@gmail.com",
                Orders = new List<Order>
                {
                    new Order
                    {
                         Id = 1,
                         CreatedDate = new System.DateTime(2018,01,12),
                          Items = new List<Product>()
                          {
                              new Product() { Name = "Monitor", Price = 80 },
                              new Product() { Name = "Keyboard", Price = 30 }
                          }
                    }
                }
            };
            var result = _service.CreateCustomer(customer);
            Assert.IsTrue(result);
        }

        [Test]
        public void CreateCustomerWithoutOrdersShouldPassTest()
        {
            Customer customer = new Customer
            {
                Name = "John",
                EmailId = "john@gmail.com"
            };
            var result = _service.CreateCustomer(customer);
            Assert.IsTrue(result);
        }
    }
}
