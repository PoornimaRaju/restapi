﻿using System;
using System.Collections.Generic;
using ECommerce.DataContracts;
using ECommerce.Service;
using NUnit.Framework;

namespace ECommerce.Test
{
    [TestFixture]
    class GetCustomersWithoutOrdersTest
    {
        private ECommerceService _service;
        
        private void CreateCustomerWithOrder()
        {
            Customer customer = new Customer
            {
                Name = "John",
                EmailId = "john@gmail.com",
                Orders = new List<Order>
                {
                    new Order
                    {
                         CreatedDate = DateTime.Now,
                         Items = new List<Product>()
                         {
                              new Product() { Name = "Monitor", Price = 80 },
                              new Product() { Name = "Keyboard", Price = 30 }
                         }
                    }
                }
            };

            _service.CreateCustomer(customer);
        }

        private void CreateCustomersWithOutOrder()
        {
            Customer customer1 = new Customer
            {
                Name = "Harry",
                EmailId = "harry@gmail.com"
            };
            Customer customer2 = new Customer
            {
                Name = "Mary",
                EmailId = "mary@gmail.com"
            };
            _service.CreateCustomer(customer1);
            _service.CreateCustomer(customer2);
        }

        [Test]
        public void GetCustomersWithoutOrdersWhenCustomersWithoutOrdersShouldPassTest()
        {
            // Setup
            _service = new ECommerceService();
            CreateCustomerWithOrder();
            CreateCustomersWithOutOrder();

            // Action
            var result = _service.GetCustomersWithoutOrders();

            // Assert
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].Id);
            Assert.AreEqual("Harry", result[0].Name);
            Assert.AreEqual("harry@gmail.com", result[0].EmailId);
            Assert.AreEqual(0, result[0].Orders.Count);

            Assert.AreEqual(2, result[1].Id);
            Assert.AreEqual("Mary", result[1].Name);
            Assert.AreEqual("mary@gmail.com", result[1].EmailId);
            Assert.AreEqual(0, result[1].Orders.Count);
        }

        [Test]
        public void GetCustomersWithoutOrdersWhenNoCustomerShouldReturnEmpty()
        {
            // Setup
            _service = new ECommerceService();

            // Action
            var result = _service.GetCustomersWithoutOrders();

            // Assert
            Assert.IsEmpty(result);
        }

        [Test]
        public void GetCustomersWithoutOrdersWhenNoCustomerWithoutOrderShouldReturnEmpty()
        {
            // Setup
            _service = new ECommerceService();
            CreateCustomerWithOrder();

            // Action
            var result = _service.GetCustomersWithoutOrders();

            // Assert
            Assert.IsEmpty(result);
        }
    }
}
