﻿using System;
using System.Collections.Generic;
using ECommerce.DataContracts;
using ECommerce.Service;
using NUnit.Framework;

namespace ECommerce.Test
{
    [TestFixture]
    public class GetCustomerTest
    {
        private ECommerceService _service;

        private void CreateCustomers()
        {
            Customer customer1 = new Customer
            {
                Name = "John",
                EmailId = "john@gmail.com",
                Orders = new List<Order>
                {
                    new Order
                    {
                         CreatedDate = DateTime.Now,
                         Items = new List<Product>()
                         {
                              new Product() { Name = "Monitor", Price = 80 },
                              new Product() { Name = "Keyboard", Price = 30 }
                         }
                    }
                }
            };
            Customer customer2 = new Customer
            {
                Name = "John",
                EmailId = "john@gmail.com",
                Orders = new List<Order>
                {
                    new Order
                    {
                         CreatedDate = DateTime.Now,
                         Items = new List<Product>()
                         {
                              new Product() { Name = "Monitor", Price = 80 },
                              new Product() { Name = "Keyboard", Price = 30 }
                         }
                    },
                     new Order
                    {
                         CreatedDate = DateTime.Now,
                         Items = new List<Product>()
                         {
                              new Product() { Name = "Cable", Price = 5 }
                         }
                    }
                }
            };
            _service.CreateCustomer(customer1);
            _service.CreateCustomer(customer2);
        }

        [Test]
        public void GetCustomerWithValidIdShouldPassTest()
        {
            _service = new ECommerceService();
            CreateCustomers();

            var result = _service.GetCustomer(1);
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("John", result.Name);
            Assert.AreEqual("john@gmail.com", result.EmailId);
            Assert.AreEqual(2, result.Orders.Count);

            //Order 1
            Assert.AreEqual(0, result.Orders[0].Id);
            Assert.AreEqual(2, result.Orders[0].Items.Count);

            // Product 1
            Assert.AreEqual("Monitor", result.Orders[0].Items[0].Name);
            Assert.AreEqual(80, result.Orders[0].Items[0].Price);

            // Product 2
            Assert.AreEqual("Keyboard", result.Orders[0].Items[1].Name);
            Assert.AreEqual(30, result.Orders[0].Items[1].Price);

            //Order 2
            Assert.AreEqual(1, result.Orders[1].Id);
            Assert.AreEqual(1, result.Orders[1].Items.Count);

            // Product 1
            Assert.AreEqual("Cable", result.Orders[1].Items[0].Name);
            Assert.AreEqual(5, result.Orders[1].Items[0].Price);
        }

        [Test]
        public void GetCustomerWithInvalidIdShouldFailTest()
        {
            _service = new ECommerceService();
            CreateCustomers();

            var result = _service.GetCustomer(2);

            Assert.IsNull(result);
        }

        [Test]
        public void GetCustomerWithOutCustomersShouldReturnNullTest()
        {
            _service = new ECommerceService();

            var result = _service.GetCustomer(0);

            Assert.IsNull(result);
        }
    }
}
