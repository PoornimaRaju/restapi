﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ECommerce.DataContracts
{
    [DataContract(Name = "customer")]
    public class Customer
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "emailId")]
        public string EmailId { get; set; }

        [DataMember(Name = "order")]
        public List<Order> Orders { get; set; }
    }
}
