﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ECommerce.DataContracts
{
    [DataContract(Name = "order")]
    public class Order
    {
        float _price = 0;

        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "items")]
        public List<Product> Items { get; set; }

        [DataMember(Name = "createdDate")]
        public DateTime CreatedDate { get; set; }

        [DataMember(Name = "price")]
        public float Price
        {
            get
            {
                foreach (Product item in Items)
                {
                    _price += item.Price;
                }
                return _price;
            }            
        }
    }
}
