﻿using System.Runtime.Serialization;

namespace ECommerce.DataContracts
{
    [DataContract(Name = "product")]
    public class Product
    {

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "price")]
        public float Price { get; set; }
    }
}
