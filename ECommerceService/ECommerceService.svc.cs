﻿using ECommerce.DataContracts;
using System;
using System.Collections.Generic;

namespace ECommerce.Service
{
    public class ECommerceService : IECommerceService
    {
        private List<Customer> customersList = new List<Customer>();
        private int customerId = 0;
        private int orderId = 0;

        #region IECommerceService Implementation
        public bool CreateCustomer(Customer customer)
        {
            bool isCustomerCreated = false;
            if (customer != null && AreOrdersValid(customer.Orders))
            {
                var newCustomer = CreateNewCustomer(customer);
                ResetOrderId();
                CreateOrdersForCustomer(newCustomer, customer.Orders);
                customersList.Add(newCustomer);
                isCustomerCreated = true;
            }
            return isCustomerCreated;
        }
        
        public bool CreateOrder(int customerId, Order order)
        {
            Customer customer = GetCustomer(customerId);
            bool isOrderCreated = false;
            if (customer != null && isOrderValid(order))
            {
                AddOrder(customer, order);
                isOrderCreated = true;
            }
            return isOrderCreated;
        }

        public Customer GetCustomer(int customerId)
        {
            return customersList.Find(x => x.Id == customerId);
        }

        public List<Customer> GetCustomersWithoutOrders()
        {
            return customersList.FindAll(x => x.Orders.Count == 0);
        }

        #endregion

        #region private methods
        private bool AreOrdersValid(List<Order> orders)
        {
            bool isValid = true;
            if (orders != null)
            {
                foreach (var order in orders)
                {
                    isValid = isValid && isOrderValid(order);
                }
            }
            return isValid;
        }

        private bool isOrderValid(Order order)
        {
            return(
                order != null && 
                order.CreatedDate != DateTime.MinValue && 
                order.Items != null &&
                order.Price > 0
            );
        }

        private Customer CreateNewCustomer(Customer customer)
        {
            Customer newCustomer = new Customer()
            {
                Id = customerId++,
                EmailId = customer.EmailId,
                Name = customer.Name,
                Orders = new List<Order>()
            };
            return newCustomer;
        }

        private void ResetOrderId()
        {
            orderId = 0;
        }

        private void CreateOrdersForCustomer(Customer customer, List<Order> orders)
        {
            if (orders != null)
            {
                foreach (var order in orders)
                {
                    AddOrder(customer, order);
                }
            }
        }

        private void AddOrder(Customer customer, Order order)
        {
            order.Id = orderId++;
            customer.Orders.Add(order);
        }

        #endregion
    }
}
