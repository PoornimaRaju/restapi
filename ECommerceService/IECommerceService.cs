﻿using ECommerce.DataContracts;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace ECommerce.Service
{
    [ServiceContract]
    public interface IECommerceService
    {
        [WebGet(UriTemplate = "users")]
        [OperationContract]
        List<Customer> GetCustomersWithoutOrders();

        [WebGet(UriTemplate = "users/{customer_id}")]
        [OperationContract]
        Customer GetCustomer(int customerId);
        
        [WebInvoke(Method = "POST", UriTemplate = "users/{customer_id}/orders")]
        [OperationContract]
        bool CreateOrder(int customerId, Order order);

        [WebInvoke(Method = "PUT", UriTemplate = "users/{customer_id}")]
        [OperationContract]
        bool CreateCustomer(Customer customer);
    }
}
